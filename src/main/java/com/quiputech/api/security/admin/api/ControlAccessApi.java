package com.quiputech.api.security.admin.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import com.quiputech.api.security.admin.factories.ControlAccessApiFactory;
import com.quiputech.security.module.entities.*;
import com.quiputech.api.security.admin.model.GenericResponse;
import com.quiputech.security.module.dto.SecurityResponse;
import io.swagger.annotations.Api;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mgaldamez
 */


@Api(value = "Security Api", description = "RESTful API to handle cognito and redhat services.")
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ControlAccessApi  {
    private final ControlAccessApiService delegate = ControlAccessApiFactory.getControlAccesApi();
    
    @GET
    @POST
    @Path("{appId}/security")
    @ApiOperation(value = "Retrieve access token", notes = "RESTful API to manage security tokens.", response = SecurityResponse.class, authorizations = {
        @Authorization(value ="basicAuth")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(
            @ApiParam(value = "Client Id", required = true, example = "acquirer") @PathParam("appId") String appId,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @GET
    @POST
    @Path("{appId}/security/accessToken")
    @ApiOperation(value = "Retrieve access token plain text", notes = "RESTful API to manage security tokens with keys.", response = SecurityResponse.class, authorizations = {
        @Authorization(value ="basicAuth")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.TEXT_PLAIN)
    public Response retrieveAccessToken(@Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @GET
    @POST
    @Path("{appId}/security/keys")
    @ApiOperation(value = "Retrieve access token with keys", notes = "RESTful API to manage security tokens with keys.", response = SecurityResponse.class, authorizations = {
        @Authorization(value ="basicAuth")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.APPLICATION_JSON)
    public Response createWithKeys(@Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @GET
    @POST
    @Path("{appId}/security/refresh")
    @ApiOperation(value = "Refresh access token", notes = "RESTful API to refresh security tokens.", response = SecurityResponse.class, authorizations = {
        @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.APPLICATION_JSON)
    public Response refresh(@Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @GET
    @POST
    @Path("{appId}/security/refresh/keys")
    @ApiOperation(value = "Refresh access token and keys", notes = "RESTful API to refresh security tokens with keys.", response = SecurityResponse.class, authorizations = {
        @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.APPLICATION_JSON)
    public Response refreshWithKeys(@Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @POST
    @Path("/security/encrypt")
    @ApiOperation(value = "Encrypt payload", notes = "RESTful API to encrypt payloads", response = String.class, authorizations = {
        @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.TEXT_PLAIN)
    public Response encrypt(@Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @POST
    @Path("/security/decrypt")
    @ApiOperation(value = "Decrypt payload", notes = "RESTful API to decrypt payloads", response = String.class, authorizations = {
        @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.TEXT_PLAIN)
    public Response decrypt(@Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @POST
    @Path("/security/base64/encode")
    @ApiOperation(value = "Encode base64", notes = "RESTful API to encode payloads", response = String.class, authorizations = {
        @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.TEXT_PLAIN)
    public Response encode(@Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @POST
    @Path("/security/base64/decode")
    @ApiOperation(value = "Encode base64", notes = "RESTful API to decode payloads", response = String.class, authorizations = {
        @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Request is not valid")})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Produces(MediaType.TEXT_PLAIN)
    public Response decode(@Context SecurityContext securityContext, @Context HttpHeaders headers) throws NotFoundException {
       return null;
    }
    
    @POST
    @Path("{appId}/create")
    @ApiOperation(value = "createUser", notes = "RESTful API to create users.", response = GenericResponse.class, authorizations = {
    @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."), 
                           @ApiResponse(code = 401, message = "UNAUTHORIZED"), 
                           @ApiResponse(code = 500, message = "REQUEST_NOT_VALID")})
    public Response createUser(
            @ApiParam(value = "Client Id", required = true, example = "acquirer") @PathParam("appId") String appId,
            @ApiParam(value = "Cognito User Request", required = true) UserRequest request,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws Exception {
        return delegate.createUser(appId, request, securityContext, headers);
    }
    
    @POST
    @Path("/{appId}/edit")
    @ApiOperation(value = "updateUser", notes = "RESTful API to update users.", response = GenericResponse.class, authorizations = {
    @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."), 
                           @ApiResponse(code = 401, message = "UNAUTHORIZED"), 
                           @ApiResponse(code = 500, message = "REQUEST_NOT_VALID")})
    public Response updateUser(
            @ApiParam(value = "Client Id", required = true, example = "j0kpn35m21r1gkkjef7vkl62c") @PathParam("appId") String appId,
            @ApiParam(value = "Cognito User Request", required = true) UserRequest request,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws Exception {
        return delegate.updateUser(appId, request, securityContext, headers);
    }
    
    @GET
    @Path("/{appId}/remove/{username}")
    @ApiOperation(value = "deleteUser", notes = "RESTful API to delete an user.", response = GenericResponse.class, authorizations = {
    @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."), 
                           @ApiResponse(code = 401, message = "UNAUTHORIZED"), 
                           @ApiResponse(code = 500, message = "REQUEST_NOT_VALID")})
    public Response deleteUser(
            @ApiParam(value = "Client Id", required = true, example = "acquirer") @PathParam("appId") String appId,
            @ApiParam(value = "Username", required = true, example = "mgaldamez") @PathParam("username") String username,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws Exception {
        return delegate.deleteUser(appId, username, securityContext, headers);
    }
    
    @POST
    @Path("/{appId}/list")
    @ApiOperation(value = "listUsers", notes = "RESTful API to list users.", response = CognitoUser.class, authorizations = {
    @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."), 
                           @ApiResponse(code = 401, message = "UNAUTHORIZED"), 
                           @ApiResponse(code = 500, message = "REQUEST_NOT_VALID")})
    public Response listUsers(
            @ApiParam(value = "Client Id", required = true, example = "acquirer") @PathParam("appId") String appId,
            @ApiParam(value = "Cognito User Filter Request", required = true) UserFilterRequest request,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws Exception {
        return delegate.listUser(appId, request, securityContext, headers);
    }
    
    @GET
    @Path("/{appId}/reset/{username}")
    @ApiOperation(value = "resetPassword", notes = "RESTful API to reset password of an user.", response = GenericResponse.class, authorizations = {
    @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."), 
                           @ApiResponse(code = 401, message = "UNAUTHORIZED"), 
                           @ApiResponse(code = 500, message = "REQUEST_NOT_VALID")})
    public Response resetPassword(
            @ApiParam(value = "Client Id", required = true, example = "acquirer") @PathParam("appId") String appId,
            @ApiParam(value = "Username", required = true, example = "mgaldamez") @PathParam("username") String username,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws Exception {
        return delegate.resetPassword(appId, username, securityContext, headers);
    }
    
    @POST
    @Path("/{appId}/change")
    @ApiOperation(value = "changePassword", notes = "RESTful API to change password of an user.", response = GenericResponse.class, authorizations = {
    @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."), 
                           @ApiResponse(code = 401, message = "UNAUTHORIZED"), 
                           @ApiResponse(code = 500, message = "REQUEST_NOT_VALID")})
    public Response changePassword(
            @ApiParam(value = "Cognito User Request", required = true) UserRequest request,
            @ApiParam(value = "Client Id", required = true, example = "acquirer") @PathParam("appId") String appId,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws Exception {
        return delegate.changePassword(appId, request, securityContext, headers);
    }
    
    @POST
    @Path("/{appId}/recover")
    @ApiOperation(value = "recoverPassword", notes = "RESTful API to recover password of an user.", response = GenericResponse.class, authorizations = {
    @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."), 
                           @ApiResponse(code = 401, message = "UNAUTHORIZED"), 
                           @ApiResponse(code = 500, message = "REQUEST_NOT_VALID")})
    public Response recoverPassword(
            @ApiParam(value = "Cognito User Request", required = true) UserRequest request,
            @ApiParam(value = "Client Id", required = true, example = "acquirer") @PathParam("appId") String appId,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws Exception {
        return delegate.confirmPassword(appId, request, securityContext, headers);
    }
    
    @POST
    @Path("/{appId}/auth")
    @ApiOperation(value = "authUser", notes = "RESTful API to authentication users.", response = CognitoAccount.class, authorizations = {
    @Authorization(value = "accessToken")
    })
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."), 
                           @ApiResponse(code = 401, message = "UNAUTHORIZED"), 
                           @ApiResponse(code = 500, message = "REQUEST_NOT_VALID")})
    public Response authentication
        (
            @ApiParam(value = "Cognito User Request", required = true) UserRequest request,
            @ApiParam(value = "Client Id", required = true, example = "acquirer") @PathParam("appId") String appId,
            @Context SecurityContext securityContext, @Context HttpHeaders headers) throws Exception {
        return delegate.authUser(appId, request, securityContext, headers);
    }
}