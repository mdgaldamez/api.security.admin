package com.quiputech.api.security.admin.api;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import com.quiputech.security.module.entities.*;
//import org.keycloak.representations.idm.UserRepresentation;
/**
 *
 * @author mgaldamez
 */
public abstract class ControlAccessApiService {
    public abstract Response createUser(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response listUser(String appId, UserFilterRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response updateUser(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response deleteUser(String appId, String username, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response resetPassword(String appId, String username, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response changePassword(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response confirmPassword(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response authUser(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response getToken(String appId, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    /*public abstract Response createRHUser(String appId, UserRepresentation body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    //public abstract Response listUser(String appId, UserFilterRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response updateRHUser(String appId, UserRepresentation body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response deleteRHUser(String appId, String username, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;
    
    public abstract Response authRHUser(String appId, String username, String password, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException;*/
}
