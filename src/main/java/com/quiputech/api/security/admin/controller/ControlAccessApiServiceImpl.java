package com.quiputech.api.security.admin.controller;

import java.util.List;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.quiputech.api.security.admin.api.ControlAccessApiService;
import com.quiputech.api.security.admin.model.*;
import com.quiputech.security.module.*;
import com.quiputech.security.module.entities.*;
import java.util.Base64;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mgaldamez
 */
public class ControlAccessApiServiceImpl extends ControlAccessApiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControlAccessApiServiceImpl.class);

    @Override
    public Response createUser(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            DatabaseManager db = new DatabaseManager();            
            GenericResponse response = new GenericResponse();
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            if (body != null) {
                Boolean result = false;
                String mode = "";
                Settings settings = db.getSettingsByClientId(appId);
                             
                if(settings.getMode() == 0)
                {
                    mode = "cognito";
                    CognitoManager.CLIENTID = settings.getClientId();
                    CognitoManager.USERPOOLID = settings.getPoold();
                    CognitoManager.REGION = settings.getRegion();

                    result = CognitoManager.createCognitoAccount(body);                    
                }else {
                    mode = "redHat";
                    RedHatManager.CLIENT_ID = settings.getClientId();
                    RedHatManager.CLIENT_SECRET = settings.getClientSecret();
                    RedHatManager.USERNAME = settings.getUsername();
                    RedHatManager.PASSWORD = settings.getPassword();
                    RedHatManager.REALM = settings.getRealm();
                    
                    result = RedHatManager.createUser(body);
                }
                                
                if(result) {
                    if(db.insertUser(appId, mode, body) > 0) {
                        response.setCode(200);
                        response.setMessage("OK - CREATED");
                        return Response.ok().entity(response).build();
                    } else {
                        return Response.status(Response.Status.BAD_GATEWAY).entity(error).build();
                    }
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response updateUser(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            DatabaseManager db = new DatabaseManager();
            GenericResponse response = new GenericResponse();
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            if (body != null) {
                Boolean result = false;
                String mode = "";
                Settings settings = db.getSettingsByClientId(appId);
                if(settings.getMode() == 0)
                {
                    mode = "cognito";
                    CognitoManager.CLIENTID = settings.getClientId();
                    CognitoManager.USERPOOLID = settings.getPoold();
                    CognitoManager.REGION = settings.getRegion();

                    result = CognitoManager.updateCognitoAccount(body);
                } else {
                    mode = "redHat";
                    RedHatManager.CLIENT_ID = settings.getClientId();
                    RedHatManager.CLIENT_SECRET = settings.getClientSecret();
                    RedHatManager.USERNAME = settings.getUsername();
                    RedHatManager.PASSWORD = settings.getPassword();
                    RedHatManager.REALM = settings.getRealm();
                    
                    result = RedHatManager.updateUser(body);          
                }
                if(result) {
                    if(db.updateUser(appId, mode, body) > 0) {
                        response.setCode(200);
                        response.setMessage("OK - UPDATED");
                        return Response.ok().entity(response).build();
                    } else {
                        return Response.status(Response.Status.BAD_GATEWAY).entity(error).build();
                    }
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response deleteUser(String appId, String username, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            DatabaseManager db = new DatabaseManager();
            GenericResponse response = new GenericResponse();
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            if (username != null) {
                Boolean result = false;
                Settings settings = db.getSettingsByClientId(appId);
                             
                if(settings.getMode() == 0)
                {
                    CognitoManager.CLIENTID = settings.getClientId();
                    CognitoManager.USERPOOLID = settings.getPoold();
                    CognitoManager.REGION = settings.getRegion();

                    result = CognitoManager.deleteCognitoAccount(username);                   
                }else {
                    RedHatManager.CLIENT_ID = settings.getClientId();
                    RedHatManager.CLIENT_SECRET = settings.getClientSecret();
                    RedHatManager.USERNAME = settings.getUsername();
                    RedHatManager.PASSWORD = settings.getPassword();
                    RedHatManager.REALM = settings.getRealm();
                    
                    result = RedHatManager.deleteUser(username);
                }

                if(result) {
                    if(db.deleteUser(appId, username) > 0) {
                        response.setCode(200);
                        response.setMessage("OK - DELETED");
                        return Response.ok().entity(response).build();
                    } else {
                        return Response.status(Response.Status.BAD_GATEWAY).entity(error).build();
                    }
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @Override
    public Response listUser(String appId, UserFilterRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            if (body != null) {
                DatabaseManager dataController = new DatabaseManager();
                Settings settings = dataController.getSettingsByClientId(appId);
                CognitoManager.CLIENTID = settings.getClientId();
                CognitoManager.USERPOOLID = settings.getPoold();
                CognitoManager.REGION = settings.getRegion();
                
                List<CognitoUser> users = CognitoManager.getListUsers(body);
                
                if(users.size() > 0) {
                    return Response.ok().entity(users).build();
                } else {
                    error.setDescription("USERS NOT FOUND");
                    return Response.status(Response.Status.OK).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response resetPassword(String appId, String username, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            GenericResponse response = new GenericResponse();
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            if (username != null) {
                DatabaseManager dataController = new DatabaseManager();
                Settings settings = dataController.getSettingsByClientId(appId);
                CognitoManager.CLIENTID = settings.getClientId();
                CognitoManager.USERPOOLID = settings.getPoold();
                CognitoManager.REGION = settings.getRegion();
                
                Boolean result = CognitoManager.resetCognitoPassword(username);
                
                if(result) {
                    response.setCode(200);
                    response.setMessage("OK - RESET_PASSWORD");
                    return Response.ok().entity(response).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response changePassword(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            GenericResponse response = new GenericResponse();
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            if (body != null) {
                DatabaseManager dataController = new DatabaseManager();
                Settings settings = dataController.getSettingsByClientId(appId);
                CognitoManager.CLIENTID = settings.getClientId();
                CognitoManager.USERPOOLID = settings.getPoold();
                CognitoManager.REGION = settings.getRegion();
                
                Boolean result = CognitoManager.changePassword(body);
                
                if(result) {
                    response.setCode(200);
                    response.setMessage("OK - UPDATED");
                    return Response.ok().entity(response).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response confirmPassword(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            GenericResponse response = new GenericResponse();
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            if (body != null) {
                DatabaseManager dataController = new DatabaseManager();
                Settings settings = dataController.getSettingsByClientId(appId);
                CognitoManager.CLIENTID = settings.getClientId();
                CognitoManager.USERPOOLID = settings.getPoold();
                CognitoManager.REGION = settings.getRegion();
                
                Boolean result = CognitoManager.confirmForgotPassword(body);
                
                if(result) {
                    response.setCode(200);
                    response.setMessage("OK - CONFIRMED");
                    return Response.ok().entity(response).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response authUser(String appId, UserRequest body, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            if (body != null) {
                DatabaseManager dataController = new DatabaseManager();
                Settings settings = dataController.getSettingsByClientId(appId);
                CognitoManager.CLIENTID = settings.getClientId();
                CognitoManager.USERPOOLID = settings.getPoold();
                CognitoManager.REGION = settings.getRegion();
                
                CognitoAccount account = CognitoManager.authenticateAccount(body.getUsername(), body.getPassword());
                if(!account.getAccessToken().isEmpty()) {
                    return Response.ok().entity(account).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @Override
    public Response getToken(String appId, SecurityContext securityContext, HttpHeaders headers) throws NotFoundException {
        try {
            ErrorMessage error = new ErrorMessage();
            error.setErrorCode(400);
            error.setDescription("Request body is wrong.");
            String auth = headers.getHeaderString(org.apache.http.HttpHeaders.AUTHORIZATION);
            if (auth != null) {
                byte[] base64decoded = Base64.getDecoder().decode(auth.replace("Basic ", ""));
                String authCredentials[] = new String(base64decoded, "UTF-8").split(":");
                DatabaseManager dataController = new DatabaseManager();
                Settings settings = dataController.getSettingsByClientId(appId);
                CognitoManager.CLIENTID = settings.getClientId();
                CognitoManager.USERPOOLID = settings.getPoold();
                CognitoManager.REGION = settings.getRegion();
                
                CognitoAccount account = CognitoManager.authenticateAccount(authCredentials[0], authCredentials[1]);
                if(!account.getAccessToken().isEmpty()) {
                    return Response.status(HttpServletResponse.SC_CREATED).type(MediaType.APPLICATION_JSON).entity(account).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                } 
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
