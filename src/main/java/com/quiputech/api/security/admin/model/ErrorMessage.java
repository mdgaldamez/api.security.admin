package com.quiputech.api.security.admin.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorMessage   {
  
  private int errorCode;
  private String description;

  public int getErrorCode() {
    return errorCode;
  }
  
  @JsonProperty("errorCode")
  public void setErrorCode(int errorCode) {
    this.errorCode = errorCode;
  }

  public String getDescription() {
    return description;
  }
  
  @JsonProperty("description")
  public void setDescription(String description) {
    this.description = description;
  }
  
}

