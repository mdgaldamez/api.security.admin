package com.quiputech.api.security.admin.model;

/**
 *
 * @author mgaldamez
 */
public class GenericResponse {
    private int code;
    private String message;
    
    /*private Map<String, String> dataMap;

    public Map<String, String> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<String, String> dataMap) {
        this.dataMap = dataMap;
    }*/

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
