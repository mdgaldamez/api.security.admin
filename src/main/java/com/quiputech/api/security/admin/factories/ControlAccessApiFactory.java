package com.quiputech.api.security.admin.factories;

import com.quiputech.api.security.admin.api.ControlAccessApiService;
import com.quiputech.api.security.admin.controller.ControlAccessApiServiceImpl;


/**
 *
 * @author mgaldamez
 */
public class ControlAccessApiFactory {
   private final static ControlAccessApiService SERVICE = new ControlAccessApiServiceImpl();

   public static ControlAccessApiService getControlAccesApi()
   {
      return SERVICE;
   }
}
