package com.quiputech.api.security.admin.api;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * @author mgaldamez
 */
@ApplicationPath("/")
public class RestApplication extends Application {
    
}
